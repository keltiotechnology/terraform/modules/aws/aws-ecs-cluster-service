# Aws ECS cluster service

## Usage

```hcl-terraform
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["keltio-vpc"]
  }
}
data "aws_subnet_ids" "compute_private_subnets" {
  vpc_id = data.aws_vpc.vpc.id
  tags   = {
    "namespace" = "keltio"
    "type"      = "compute"
    "tier"      = "Private"
  }
}
data "aws_security_group" "ecs_security_group" {
  name = "keltio-ecs"
}
data "aws_secretsmanager_secret" "SECRET_KEY" {
  name  = "PROD/SECRET_KEY"
}
resource "aws_service_discovery_private_dns_namespace" "ecs_service_private_dns_namespace" {
  name        = "keltio.local"
  description = "Managed by terraform"
  vpc         = data.aws_vpc.vpc.id
}

module "keltio_app_service" {
  source                  = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-ecs-cluster-service?ref=v1.0.0"

  // Global settings
  ecs_cluster_name                    = "keltio_cluster"
  ecs_service_name                    = "keltio_app_svc"

  // Network settings
  ecs_service_subnets                 = data.aws_subnet_ids.compute_private_subnets.ids
  ecs_service_security_groups         = [data.aws_security_group.ecs_security_group.id]
  ecs_service_registries_enabled      = true
  ecs_service_registries              = {
    container_name           = "app"
    discovery_service_name   = "app"
    private_dns_namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_private_dns_namespace.id
  }

  // Compute settings
  ecs_service_task_family             = "keltio-app"
  ecs_service_task_reserved_cpu       = 256
  ecs_service_task_reserved_memory    = 512
  ecs_service_task_containers         = [{
      container_name        = "app"
      container_image_name  = "nginx"
      container_image_tag   = "latest"
      container_command     = []
      container_cpu         = 256
      container_memory      = 512
      container_port        = 80
      host_port             = 80
      env_vars              = {
        "ENV": "prod"
      }
      env_secrets           = {
        "SECRET_KEY"               : data.aws_secretsmanager_secret.SECRET_KEY.arn
      }
      volumes               = [{
          "name": "app-media",
          "file_system_id": "fs-XXXXXXX",
          "container_path": "/media",
          "access_point_id": "fsap-0208e68ceeb9b92c2",
          "encryption_in_transit": "ENABLED",
          "iam_efs_autorisation": "ENABLED"
      }]
      journaux              = {
        "awslogs-group"        : "/keltio/app"
        "awslogs-region"       :	"eu-west-3"
        "awslogs-stream-prefix": "keltio-app"
      }
      depends_on            = []
    }
  ]

  // Security
  ecs_service_task_execution_role_name                = "AppEcsTaskExecutionRole"
  ecs_service_task_execution_role_policy_name         = "AppEcsTaskExecutionRolePolicy"
  ecs_service_task_execution_role_allowed_secrets_arn = [
    data.aws_secretsmanager_secret.SECRET_KEY.arn
  ]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.1 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.68.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_container_definition"></a> [container\_definition](#module\_container\_definition) | cloudposse/ecs-container-definition/aws | 0.57.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.ecs_log_groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_service.ecs_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition.service_task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_iam_role.ecs_service_task_execution_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.ecs_service_task_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.ecs_service_ssm_task_role_policy_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.ecs_service_task_execution_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.ecs_service_task_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.ecs_service_task_execution_role_policy_attachment_ec2pull](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.ecs_service_task_execution_role_policy_attachment_ecrpull](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_service_discovery_service.ecs_service_discovery_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/service_discovery_service) | resource |
| [aws_ecs_cluster.ecs_cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_cluster) | data source |
| [aws_iam_policy_document.ecs_task_assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_role.ecs_service_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ecs_cluster_region"></a> [ecs\_cluster\_region](#input\_ecs\_cluster\_region) | Region where is located the ECS cluster | `string` | n/a | yes |
| <a name="input_ecs_cluster_name"></a> [ecs\_cluster\_name](#input\_ecs\_cluster\_name) | ECS cluster name of the app | `string` | `"ecs"` | no |
| <a name="input_ecs_service_name"></a> [ecs\_service\_name](#input\_ecs\_service\_name) | ECS service name | `string` | n/a | yes |
| <a name="input_ecs_service_desired_count"></a> [ecs\_service\_desired\_count](#input\_ecs\_service\_desired\_count) | ECS service desired count | `number` | `1` | no |
| <a name="input_ecs_service_task_family"></a> [ecs\_service\_task\_family](#input\_ecs\_service\_task\_family) | ECS service task family | `string` | n/a | yes |
| <a name="input_ecs_service_vpc"></a> [ecs\_service\_vpc](#input\_ecs\_service\_vpc) | ECS service vpc | `string` | n/a | yes |
| <a name="input_ecs_service_subnets"></a> [ecs\_service\_subnets](#input\_ecs\_service\_subnets) | ECS service subnets | `list(string)` | n/a | yes |
| <a name="input_ecs_service_security_groups"></a> [ecs\_service\_security\_groups](#input\_ecs\_service\_security\_groups) | ECS service security groups | `list(string)` | n/a | yes |
| <a name="input_ecs_service_assign_public_ip"></a> [ecs\_service\_assign\_public\_ip](#input\_ecs\_service\_assign\_public\_ip) | ECS service assign public ip | `bool` | `true` | no |
| <a name="input_ecs_service_load_balancer_enabled"></a> [ecs\_service\_load\_balancer\_enabled](#input\_ecs\_service\_load\_balancer\_enabled) | Determine if ecs service will have load balancer | `bool` | `false` | no |
| <a name="input_ecs_service_load_balancer"></a> [ecs\_service\_load\_balancer](#input\_ecs\_service\_load\_balancer) | n/a | <pre>list(object({<br>    target_group_arn = string<br>    container_name   = string<br>    container_port   = number<br>  }))</pre> | <pre>[]</pre> | no |
| <a name="input_ecs_service_registries_enabled"></a> [ecs\_service\_registries\_enabled](#input\_ecs\_service\_registries\_enabled) | Determine if ecs service discovery service will have enabled | `bool` | `false` | no |
| <a name="input_ecs_service_registries"></a> [ecs\_service\_registries](#input\_ecs\_service\_registries) | n/a | <pre>object({<br>    discovery_service_name   = string<br>    private_dns_namespace_id = string<br>    container_name           = string<br>  })</pre> | <pre>{<br>  "container_name": "to change when service registries is enabled",<br>  "discovery_service_name": "to change when service registries is enabled",<br>  "private_dns_namespace_id": "to change when service registries is enabled (Example: example.terraform.local)"<br>}</pre> | no |
| <a name="input_ecs_service_launch_type"></a> [ecs\_service\_launch\_type](#input\_ecs\_service\_launch\_type) | App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2 | `string` | `"FARGATE"` | no |
| <a name="input_ecs_service_task_launch_type"></a> [ecs\_service\_task\_launch\_type](#input\_ecs\_service\_task\_launch\_type) | App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2 | `string` | `"FARGATE"` | no |
| <a name="input_ecs_service_task_network_mode"></a> [ecs\_service\_task\_network\_mode](#input\_ecs\_service\_task\_network\_mode) | ECS service task network mode | `string` | `"awsvpc"` | no |
| <a name="input_ecs_service_task_reserved_cpu"></a> [ecs\_service\_task\_reserved\_cpu](#input\_ecs\_service\_task\_reserved\_cpu) | ECS service task reserved cpu | `string` | `256` | no |
| <a name="input_ecs_service_task_reserved_memory"></a> [ecs\_service\_task\_reserved\_memory](#input\_ecs\_service\_task\_reserved\_memory) | ECS service task reserved memory | `string` | `512` | no |
| <a name="input_ecs_service_health_check_grace_period_seconds"></a> [ecs\_service\_health\_check\_grace\_period\_seconds](#input\_ecs\_service\_health\_check\_grace\_period\_seconds) | Health Check grace period secods | `number` | `60` | no |
| <a name="input_ecs_service_task_containers"></a> [ecs\_service\_task\_containers](#input\_ecs\_service\_task\_containers) | ECS service tasks definition | <pre>list(object({<br>    container_name       = string<br>    container_image_name = string<br>    container_image_tag  = string<br>    container_command    = optional(list(string))<br>    container_cpu        = number<br>    container_memory     = number<br><br>    # container_port = optional(number)<br>    # host_port      = optional(number)<br>    port_mappings = optional(list(object({<br>      containerPort = number<br>      hostPort      = number<br>      protocol      = string<br>    })))<br><br>    essential = optional(bool)<br><br>    env_vars    = optional(map(string))<br>    env_secrets = optional(map(string))<br><br>    mount_points = list(object({<br>      name           = string<br>      container_path = string<br>      read_only      = bool<br>    }))<br><br>    volumes_from = list(object({<br>      source_container = string<br>      read_only        = bool<br>    }))<br><br>    journaux = object({<br>      awslogs-group         = string<br>      awslogs-region        = string<br>      awslogs-stream-prefix = string<br>    })<br><br>    depends_on = list(object({<br>      container_name = string<br>      condition      = string<br>    }))<br>  }))</pre> | n/a | yes |
| <a name="input_ecs_service_task_volumes"></a> [ecs\_service\_task\_volumes](#input\_ecs\_service\_task\_volumes) | All volumes info to link to ecs service task | <pre>list(object({<br>    id                    = string<br>    name                  = string<br>    arn                   = string<br>    encryption_in_transit = string<br>    iam_efs_autorisation  = string<br>    access_point_id       = string<br>    access_point_arn      = string<br>  }))</pre> | n/a | yes |
| <a name="input_ecs_service_role_name"></a> [ecs\_service\_role\_name](#input\_ecs\_service\_role\_name) | ECS service role name | `string` | `"ecsInstanceRole"` | no |
| <a name="input_ecs_service_task_role_name"></a> [ecs\_service\_task\_role\_name](#input\_ecs\_service\_task\_role\_name) | ECS service task role name | `string` | `"ecsTaskRole"` | no |
| <a name="input_ecs_service_task_role_policy_name"></a> [ecs\_service\_task\_role\_policy\_name](#input\_ecs\_service\_task\_role\_policy\_name) | ECS service task role policy name | `string` | `"ecsTaskRolePolicy"` | no |
| <a name="input_ecs_service_ssm_task_role_policy_name"></a> [ecs\_service\_ssm\_task\_role\_policy\_name](#input\_ecs\_service\_ssm\_task\_role\_policy\_name) | ECS service ssm task role policy name | `string` | `"ecsSsmTaskRolePolicy"` | no |
| <a name="input_ecs_service_task_execution_role_name"></a> [ecs\_service\_task\_execution\_role\_name](#input\_ecs\_service\_task\_execution\_role\_name) | ECS service task execution role name | `string` | `"ecsTaskExecutionRole"` | no |
| <a name="input_ecs_service_task_execution_role_policy_name"></a> [ecs\_service\_task\_execution\_role\_policy\_name](#input\_ecs\_service\_task\_execution\_role\_policy\_name) | ECS service iam task execution role policy name | `string` | `"EcsTaskExecutionRolePolicy"` | no |
| <a name="input_ecs_service_task_execution_role_allowed_secrets_arn"></a> [ecs\_service\_task\_execution\_role\_allowed\_secrets\_arn](#input\_ecs\_service\_task\_execution\_role\_allowed\_secrets\_arn) | A list of AWS secret that ecs task will be allowed to get during task execution | `list(string)` | `[]` | no |
| <a name="input_cloudwatch_log_retention_in_days"></a> [cloudwatch\_log\_retention\_in\_days](#input\_cloudwatch\_log\_retention\_in\_days) | Cloudwatch log retention for the service (in days) | `number` | `30` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Farget info
Supported task CPU and memory values for tasks that are hosted on Fargate are as follows.

CPU value	Memory value (MiB)

256 (.25 vCPU)	512 (0.5GB), 1024 (1GB), 2048 (2GB)

512 (.5 vCPU)	1024 (1GB), 2048 (2GB), 3072 (3GB), 4096 (4GB)

1024 (1 vCPU)	2048 (2GB), 3072 (3GB), 4096 (4GB), 5120 (5GB), 6144 (6GB), 7168 (7GB), 8192 (8GB)

2048 (2 vCPU)	Between 4096 (4GB) and 16384 (16GB) in increments of 1024 (1GB)

4096 (4 vCPU)	Between 8192 (8GB) and 30720 (30GB) in increments of 1024 (1GB)

https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html
