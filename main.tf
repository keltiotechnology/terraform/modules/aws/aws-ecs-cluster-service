####################################################################
#
# Load AWS data
#
####################################################################
data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = var.ecs_cluster_name
}

data "aws_iam_role" "ecs_service_role" {
  count = var.ecs_service_launch_type == "EC2" ? 1 : 0
  name  = var.ecs_service_role_name
}


####################################################################
#
# Application logging settings
#
####################################################################
locals {
  ecs_journaux            = var.ecs_service_task_containers.*.journaux
  ecs_journaux_log_groups = distinct(local.ecs_journaux.*.awslogs-group)
}
resource "aws_cloudwatch_log_group" "ecs_log_groups" {
  count = length(local.ecs_journaux_log_groups)

  name              = local.ecs_journaux_log_groups[count.index]
  retention_in_days = var.cloudwatch_log_retention_in_days
}


####################################################################
#
# Application ECS settings
#
####################################################################
module "container_definition" {
  count = length(var.ecs_service_task_containers)

  source          = "cloudposse/ecs-container-definition/aws"
  version         = "0.57.0"
  container_image = "${var.ecs_service_task_containers[count.index].container_image_name}:${var.ecs_service_task_containers[count.index].container_image_tag}"
  container_name  = var.ecs_service_task_containers[count.index].container_name

  container_cpu    = var.ecs_service_task_containers[count.index].container_cpu
  container_memory = var.ecs_service_task_containers[count.index].container_memory

  essential       = lookup(var.ecs_service_task_containers[count.index], "essential", true)
  port_mappings   = var.ecs_service_task_containers[count.index].port_mappings
  map_environment = lookup(var.ecs_service_task_containers[count.index], "env_vars", {})
  map_secrets     = lookup(var.ecs_service_task_containers[count.index], "env_secrets", {})

  command = lookup(var.ecs_service_task_containers[count.index], "container_command", [])

  mount_points = [
    for mount_point in var.ecs_service_task_containers[count.index].mount_points : {
      containerPath = lookup(mount_point, "container_path")
      sourceVolume  = lookup(mount_point, "name")
      readOnly      = tobool(lookup(mount_point, "read_only", null))
    }
  ]

  volumes_from = [
    for volume in var.ecs_service_task_containers[count.index].volumes_from : {
      sourceContainer = volume.source_container
      readOnly        = tobool(lookup(volume, "read_only", null))
    }
  ]

  log_configuration = {
    "logDriver" : "awslogs",
    "options" : var.ecs_service_task_containers[count.index].journaux
  }

  container_depends_on = [
    for dependencie in lookup(var.ecs_service_task_containers[count.index], "depends_on", []) : {
      containerName = lookup(dependencie, "container_name")
      condition     = lookup(dependencie, "condition", "SUCESS")
    }
  ]
}

resource "aws_ecs_task_definition" "service_task" {
  family = var.ecs_service_task_family

  cpu                   = var.ecs_service_task_reserved_cpu
  memory                = var.ecs_service_task_reserved_memory
  container_definitions = jsonencode(module.container_definition.*.json_map_object)

  dynamic "volume" {
    for_each = var.ecs_service_task_volumes
    iterator = volume

    content {
      name = volume.value.name

      efs_volume_configuration {
        file_system_id = volume.value.id

        transit_encryption = volume.value.encryption_in_transit
        authorization_config {
          access_point_id = volume.value.access_point_id
          iam             = volume.value.iam_efs_autorisation
        }
      }
    }
  }

  requires_compatibilities = [var.ecs_service_task_launch_type]
  network_mode             = var.ecs_service_task_network_mode

  task_role_arn      = aws_iam_role.ecs_service_task_role.arn
  execution_role_arn = aws_iam_role.ecs_service_task_execution_role.arn
}

resource "aws_service_discovery_service" "ecs_service_discovery_service" {
  count = var.ecs_service_registries_enabled ? 1 : 0

  name = var.ecs_service_registries.discovery_service_name

  dns_config {
    namespace_id = var.ecs_service_registries.private_dns_namespace_id

    dns_records {
      ttl  = 60
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_ecs_service" "ecs_service" {
  cluster                           = data.aws_ecs_cluster.ecs_cluster.id
  launch_type                       = var.ecs_service_launch_type
  enable_execute_command            = true
  health_check_grace_period_seconds = var.ecs_service_health_check_grace_period_seconds

  name            = var.ecs_service_name
  task_definition = aws_ecs_task_definition.service_task.arn
  desired_count   = var.ecs_service_desired_count


  // Do not set when ecs_service_launch_type is FARGATE
  iam_role = var.ecs_service_launch_type == "EC2" ? data.aws_iam_role.ecs_service_role[0].arn : null

  dynamic "network_configuration" {
    for_each = var.ecs_service_launch_type == "FARGATE" ? ["NETWORK_CONFIGURATION_REQUIRED"] : []

    content {
      subnets          = var.ecs_service_subnets
      security_groups  = var.ecs_service_security_groups
      assign_public_ip = var.ecs_service_assign_public_ip
    }
  }

  dynamic "load_balancer" {
    for_each = var.ecs_service_load_balancer
    iterator = ecs_service_load_balancer

    content {
      target_group_arn = ecs_service_load_balancer.value.target_group_arn
      container_name   = ecs_service_load_balancer.value.container_name
      container_port   = ecs_service_load_balancer.value.container_port
    }
  }

  dynamic "service_registries" {
    for_each = var.ecs_service_registries_enabled ? ["SERVICE_REGISTRIES_ENABLED"] : []

    content {
      registry_arn   = aws_service_discovery_service.ecs_service_discovery_service[0].arn
      container_name = var.ecs_service_registries.container_name
    }
  }
}
