resource "aws_iam_role" "ecs_service_task_role" {
  name               = var.ecs_service_task_role_name
  assume_role_policy = data.aws_iam_policy_document.ecs_task_assume_role_policy.json
}

data "aws_iam_policy_document" "ecs_task_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy" "ecs_service_task_role_policy" {
  count = length(var.ecs_service_task_volumes) > 0 ? 1 : 0

  name = var.ecs_service_task_role_policy_name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        // "Condition" : {
        //   "StringLike" : {
        //     "elasticfilesystem:AccessPointArn" : var.ecs_service_task_volumes.*.access_point_arn
        //   }
        // },
        "Action" : [
          "elasticfilesystem:ClientMount",
          "elasticfilesystem:ClientWrite",
          "elasticfilesystem:ClientRootAccess"
        ],
        "Resource" : var.ecs_service_task_volumes.*.arn,
        "Effect" : "Allow",
        "Principal" : {}
      }
    ]
  })
  role = aws_iam_role.ecs_service_task_role.id
}

resource "aws_iam_role_policy" "ecs_service_ssm_task_role_policy_name" {
  name = var.ecs_service_ssm_task_role_policy_name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [{
      "Effect" : "Allow",
      "Action" : [
        "ssmmessages:CreateControlChannel",
        "ssmmessages:CreateDataChannel",
        "ssmmessages:OpenControlChannel",
        "ssmmessages:OpenDataChannel"
      ],
      "Resource" : "*"
    }]
  })
  role = aws_iam_role.ecs_service_task_role.id
}

#* https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html
resource "aws_iam_role" "ecs_service_task_execution_role" {
  name               = var.ecs_service_task_execution_role_name
  assume_role_policy = data.aws_iam_policy_document.ecs_task_assume_role_policy.json
}

resource "aws_iam_role_policy" "ecs_service_task_execution_role_policy" {
  count = length(var.ecs_service_task_execution_role_allowed_secrets_arn) > 0 ? 1 : 0

  name = var.ecs_service_task_execution_role_policy_name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "secretsmanager:GetSecretValue"
        ],
        "Resource" : var.ecs_service_task_execution_role_allowed_secrets_arn
      }
    ]
    }
  )
  role = aws_iam_role.ecs_service_task_execution_role.id
}

#* This AWS managed policy allows pulling private repo from ECR
resource "aws_iam_role_policy_attachment" "ecs_service_task_execution_role_policy_attachment_ecrpull" {
  role       = aws_iam_role.ecs_service_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

#* This AWS managed policy allows pushing private repo from ECR
resource "aws_iam_role_policy_attachment" "ecs_service_task_execution_role_policy_attachment_ec2pull" {
  role       = aws_iam_role.ecs_service_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
