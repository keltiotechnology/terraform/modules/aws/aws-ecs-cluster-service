terraform {
  # Optional attributes and the defaults function are
  # both experimental, so we must opt in to the experiment.
  experiments = [module_variable_optional_attrs]
}

# Input for ECS service
#################################################
variable "ecs_cluster_region" {
  type        = string
  description = "Region where is located the ECS cluster"
}

variable "ecs_cluster_name" {
  type        = string
  description = "ECS cluster name of the app"
  default     = "ecs"
}

variable "ecs_service_name" {
  type        = string
  description = "ECS service name"
}

variable "ecs_service_desired_count" {
  type        = number
  description = "ECS service desired count"
  default     = 1
}

variable "ecs_service_task_family" {
  type        = string
  description = "ECS service task family"
}

variable "ecs_service_vpc" {
  type        = string
  description = "ECS service vpc"
}

variable "ecs_service_subnets" {
  type        = list(string)
  description = "ECS service subnets"
}

variable "ecs_service_security_groups" {
  type        = list(string)
  description = "ECS service security groups"
}

variable "ecs_service_assign_public_ip" {
  type        = bool
  description = "ECS service assign public ip"
  default     = true
}

variable "ecs_service_load_balancer_enabled" {
  type        = bool
  description = "Determine if ecs service will have load balancer"
  default     = false
}

variable "ecs_service_load_balancer" {
  type = list(object({
    target_group_arn = string
    container_name   = string
    container_port   = number
  }))
  default = []

  # example = [{
  #   target_group_arn = "to change when ecs_service_load_balancer_enabled"
  #   container_name   = "to change when ecs_service_load_balancer_enabled"
  #   container_port   = 0
  # }]
}

variable "ecs_service_registries_enabled" {
  type        = bool
  description = "Determine if ecs service discovery service will have enabled"
  default     = false
}

variable "ecs_service_registries" {
  type = object({
    discovery_service_name   = string
    private_dns_namespace_id = string
    container_name           = string
  })
  default = {
    discovery_service_name   = "to change when service registries is enabled"
    private_dns_namespace_id = "to change when service registries is enabled (Example: example.terraform.local)"
    container_name           = "to change when service registries is enabled"
  }
}

variable "ecs_service_launch_type" {
  type        = string
  description = "App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2"
  default     = "FARGATE"
}

variable "ecs_service_task_launch_type" {
  type        = string
  description = "App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2"
  default     = "FARGATE"
}

variable "ecs_service_task_network_mode" {
  type        = string
  description = "ECS service task network mode"
  default     = "awsvpc"
}

variable "ecs_service_task_reserved_cpu" {
  type        = string
  description = "ECS service task reserved cpu"
  default     = 256
}

variable "ecs_service_task_reserved_memory" {
  type        = string
  description = "ECS service task reserved memory"
  default     = 512
}

variable "ecs_service_health_check_grace_period_seconds" {
  type        = number
  description = "Health Check grace period secods"
  default     = 60
}

variable "ecs_service_task_containers" {
  type = list(object({
    container_name       = string
    container_image_name = string
    container_image_tag  = string
    container_command    = optional(list(string))
    container_cpu        = number
    container_memory     = number

    # container_port = optional(number)
    # host_port      = optional(number)
    port_mappings = optional(list(object({
      containerPort = number
      hostPort      = number
      protocol      = string
    })))

    essential = optional(bool)

    env_vars    = optional(map(string))
    env_secrets = optional(map(string))

    mount_points = list(object({
      name           = string
      container_path = string
      read_only      = bool
    }))

    volumes_from = list(object({
      source_container = string
      read_only        = bool
    }))

    journaux = object({
      awslogs-group         = string
      awslogs-region        = string
      awslogs-stream-prefix = string
    })

    depends_on = list(object({
      container_name = string
      condition      = string
    }))
  }))
  description = "ECS service tasks definition"
}

variable "ecs_service_task_volumes" {
  type = list(object({
    id                    = string
    name                  = string
    arn                   = string
    encryption_in_transit = string
    iam_efs_autorisation  = string
    access_point_id       = string
    access_point_arn      = string
  }))
  description = "All volumes info to link to ecs service task"
}

# Input for IAM
#################################################
variable "ecs_service_role_name" {
  type        = string
  description = "ECS service role name"
  default     = "ecsInstanceRole"
}

variable "ecs_service_task_role_name" {
  type        = string
  description = "ECS service task role name"
  default     = "ecsTaskRole"
}

variable "ecs_service_task_role_policy_name" {
  type        = string
  description = "ECS service task role policy name"
  default     = "ecsTaskRolePolicy"
}

variable "ecs_service_ssm_task_role_policy_name" {
  type        = string
  description = "ECS service ssm task role policy name"
  default     = "ecsSsmTaskRolePolicy"
}

variable "ecs_service_task_execution_role_name" {
  type        = string
  description = "ECS service task execution role name"
  default     = "ecsTaskExecutionRole"
}

variable "ecs_service_task_execution_role_policy_name" {
  type        = string
  description = "ECS service iam task execution role policy name"
  default     = "EcsTaskExecutionRolePolicy"
}

variable "ecs_service_task_execution_role_allowed_secrets_arn" {
  type        = list(string)
  description = "A list of AWS secret that ecs task will be allowed to get during task execution"
  default     = []
}


# Input for Monitoring
#################################################
variable "cloudwatch_log_retention_in_days" {
  type        = number
  description = "Cloudwatch log retention for the service (in days)"
  default     = 30
}
